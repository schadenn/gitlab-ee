export default () => ({
  paths: {
    head: null,
    base: null,
  },

  isLoading: false,
  hasError: false,

  newIssues: [],
  resolvedIssues: [],
  allIssues: [],
});
